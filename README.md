# Google Cloud Managed Service for Prometheus - [セルフデプロイ コレクション](https://cloud.google.com/stackdriver/docs/managed-prometheus/setup-unmanaged?hl=ja)

## コレクタを利用する

### クラウドにデータを送信するためのサービスアカウントを作成

```sh
export project_id=$( gcloud config get-value project )
gcloud iam service-accounts create gmp-collector \
    --display-name "SA for GMP collector" \
    --description "Service Account for GMP collector"
gcloud projects add-iam-policy-binding "${project_id}" \
    --member "serviceAccount:gmp-collector@${project_id}.iam.gserviceaccount.com" \
    --role "roles/monitoring.metricWriter"
gcloud iam service-accounts keys create cred.json \
    --iam-account "gmp-collector@${project_id}.iam.gserviceaccount.com"
```

### サンプルアプリケーション / Exporter の起動

[Docker の設定を変更して](https://docs.docker.com/config/daemon/prometheus/)再起動した上で、いくつかコンテナを起動したり停止したりしてみましょう。

```sh
docker run --name nginx -d -p 8080:80 nginx
docker run --name mysql -d --net=host -e MYSQL_ROOT_PASSWORD=mysql mysql
docker run --name postgres -d --net=host -e POSTGRES_PASSWORD=password postgres
docker run --name postgres-exporter -d --net=host \
  -e DATA_SOURCE_NAME="postgresql://postgres:password@localhost:5432/postgres?sslmode=disable" \
  quay.io/prometheuscommunity/postgres-exporter
docker run --name node-exporter -d --net=host --pid=host -v "/:/host:ro,rslave" \
  quay.io/prometheus/node-exporter:latest --path.rootfs=/host
docker run --name gpu-exporter -d --gpus all --rm -p 9400:9400 \
  nvcr.io/nvidia/k8s/dcgm-exporter:3.1.6-3.1.3-ubuntu20.04
```

### [prometheus.yaml](https://prometheus.io/docs/prometheus/latest/configuration/configuration/) を編集

```sh
vim prometheus.yaml
```

### コレクタのビルドと起動

```sh
git clone -b "v2.35.0-gmp.3" --depth 1 https://github.com/GoogleCloudPlatform/prometheus
cd prometheus
make build && make tarball

./prometheus \
    --export.credentials-file cred.json\
    --config.file prometheus.yaml \
    --export.label.project-id "${project_id}" \
    --export.label.location asia-northeast1 \
    --export.compression gzip
```
